FStore / FileBucket
==========
FileBucket stores your files to the Google Cloud Platform.

DEMO URL: https://web-dot-fstore-web.appspot.com/

<!-- toc -->
# Table of Contents
* [Overview](#overview)
* [Development](#development)
* [Build](#build)
* [Deployment](#deployment)
* [Secrets](#secrets)
<!-- tocstop -->

<!-- overview -->
# Overview
**FileBucket** is a cloud storage application that has three main components:

### Web Application
- Production URL: https://web-dot-fstore-web.appspot.com/

### Server Application 
- Production URL: https://server-dot-fstore-web.appspot.com/graphql

### Command-line Interface
- See more [README](https://gitlab.com/ffueconcillo/fstore/tree/master/cli) of fstore-cli for all available commands
- See [Build](#build) on how to build the fstore-cli tarball file.

## Technologies used
### Web
- NodeJS v12.8.0
- ReactJS
- Apollo Client
- GraphQL
- [Material UI for React](https://material-ui.com/components/app-bar/)

### Server
- NodeJS v12.8.0
- Express
- Apollo Server
- GraphQL
- Google Cloud Storage

### CLI
- NodeJS v12.8.0
- Oclif

### Deployments
- Docker Swarm
- GCP - App Engines
- GCP Kubernetes

<!-- overviewstop -->


<!-- development -->
# Development

## Install dependencies
- NodeJS >12.8.0 
- Yarn >1.12.3
- Oclif dev-cli - for packaging the CLI
- cURL for fstore-cli uploading
- Google Cloud SDK (if chosen deployment will be on GCP)
```
$ npm install -g @oclif/dev-cli
```
## Clone repositories, install node modules
```
$ git clone git@gitlab.com:ffueconcillo/fstore.git
$ cd fstore/web
$ yarn install
$ cd ../server
$ yarn install
$ cd ../cli
$ yarn install
```
## Start Web and Server applications

NOTES: 
- Download secret file *fstore-d6b6f00c08dd.json* and move to */fstore/server/* See [Secrets](#secrets)
- You can optionally create a new GCP Project with Storage enabled.
- Change **FSTORE_SERVER_URL** variable to http://localhost:4000/graphql in fstore/web/src/apollo.js. 

### Server

```
$ cd fstore/server
$ npm start
```
The fstore-server application will  be at http://localhost:4000/graphql

### Web
```
$ cd fstore/web
$ npm start
```
The fstore-web application will  be at http://localhost:3000

### CLI
- Use fstore-cli to check if server component is running
- Set environment variable **FSTORE_SERVER_URL** to http://localhost:4000/graphql
```
$ cd fstore/cli
$ export FSTORE_SERVER_URL=http://localhost:4000/graphql
$ ./bin/run help
```

<!-- developmentstop -->


<!-- build -->
# Build

## Build docker images for Web and Server components
```
$ cd fstore/server
$ docker build -t francisfueconcillo/fstore-server:latest .
$ docker push francisfueconcillo/fstore-server:latest
$ cd ../web
$ npm run build
$ docker build -t francisfueconcillo/fstore-web:latest .
$ docker push francisfueconcillo/fstore-web:latest
```

## Build CLI package

Make user the @oclif/dev-cli is installed

```
$ npm install -g @oclif/dev-cli
$ cd fstore/cli
$ npm run prepack
$ npm run pack
$ npm run postpack
```
Standalone tarballs are generated at **fstore/cli/dist**. These include the node binary so the user does not have to already have node installed to use the CLI. It won't put this node binary on the PATH so the binary will not conflict with any node installation on the machine.

- Unpack the tarball and locate **/fstore-cli/bin/fstore-cli** executable, add this in your PATH
- Set environment variable **FSTORE_SERVER_URL** to https://server-dot-fstore-web.appspot.com/graphql

<!-- buildstop -->


<!-- deployment -->
# Deployment
Deployment can be done in multiple ways:
- Docker Swarm
- GCP App Engine service
- GCP Kubernetes

## UsingDocker Swarm

### Pre-requisites
- Server with Docker installed and Docker Swarm enabled
```
$ docker swarm init
```

### Deploying the fstore stack
```
$ wget https://gitlab.com/ffueconcillo/fstore/raw/master/deploy/docker-swarm/stack.yaml
$ docker stack deploy -f stack.yaml fstore
```

## Google Cloud App Engine

### Pre-requisites
- Create a GCP Project
- Enable Storage and App Engine

```
$ cd fstore/server
$ gcloud init  # create gcp configuration
$ gcloud app deploy server-app.yaml  # deploys server app service
$ cd ../web
$ gcloud app deploy web-app.yaml  # deploys web app service
```
You can check the running services at GCP
![Google Cloud App Engine](https://gitlab.com/ffueconcillo/fstore/raw/master/gcp-screenshot.png)


## Google Kubernetes Engine GKE

### Pre-requisites
- Install [Kubernetes CLI](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
- Create a GCP Project
- Enable Storage and Kubernetes Engine
- Create Kubernetes Cluster and connect to cluster
```
$ cd fstore/deployment/k8
$ kubectl apply -f deployment.yaml --record
$ kubectl get deployments  # lists successfule deployments
$ kubectl get services  # lists running services and their ports
```
You can check the running K8 services at GCP
![GKE](https://gitlab.com/ffueconcillo/fstore/raw/master/k8-screenshot.png)

<!-- deploymentstop -->


<!-- secrets -->
## Secrets
The following are not meant to be shared but for DEMO PURPOSES secret files are provided here

[fstore-d6b6f00c08dd.json] (https://storage.cloud.google.com/fstore-secrets/fstore-d6b6f00c08dd.json)
<!-- secretsstop -->