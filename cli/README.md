fstore-cli
==========

FileStorage CLI

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage

Set environment variable **FSTORE_SERVER_URL** to https://server-dot-fstore-web.appspot.com/graphql

<!-- usage -->
```sh-session
$ npm install -g fstore-cli
$ fstore-cli COMMAND
running command...
$ fstore-cli (-v|--version|version)
fstore-cli/0.0.2 darwin-x64 node-v10.1.0
$ fstore-cli --help [COMMAND]
USAGE
  $ fstore-cli COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`fstore-cli deleteFile`](#fstore-cli-deletefile)
* [`fstore-cli help [COMMAND]`](#fstore-cli-help-command)
* [`fstore-cli listFiles`](#fstore-cli-listfiles)
* [`fstore-cli uploadFile`](#fstore-cli-uploadfile)

## `fstore-cli deleteFile`

Deletes a file in FileBucket

```
USAGE
  $ fstore-cli deleteFile

OPTIONS
  -f, --file=file  Name of file to delete

DESCRIPTION
  ...
  Deletes a single file in FileBucket
```

_See code: [src/commands/deleteFile.js](https://github.com/fstore/fstore-cli/blob/v0.0.2/src/commands/deleteFile.js)_

## `fstore-cli help [COMMAND]`

display help for fstore-cli

```
USAGE
  $ fstore-cli help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.2.3/src/commands/help.ts)_

## `fstore-cli listFiles`

Lists all files in FileBucket

```
USAGE
  $ fstore-cli listFiles

DESCRIPTION
  ...
  Lists all files in FileBucket
```

_See code: [src/commands/listFiles.js](https://github.com/fstore/fstore-cli/blob/v0.0.2/src/commands/listFiles.js)_

## `fstore-cli uploadFile`

Uploads a file to FileBucket

```
USAGE
  $ fstore-cli uploadFile

OPTIONS
  -f, --file=file  Path of file to upload

DESCRIPTION
  ...
  Uploads a single file to FileBucket
```

_See code: [src/commands/uploadFile.js](https://github.com/fstore/fstore-cli/blob/v0.0.2/src/commands/uploadFile.js)_
<!-- commandsstop -->
