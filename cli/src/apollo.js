const { ApolloClient } = require('apollo-client');
const { InMemoryCache } = require('apollo-cache-inmemory');
const { createUploadLink } = require('apollo-upload-client');
const fetch = require('cross-fetch');

const link = createUploadLink({ fetch, uri: process.env.FSTORE_SERVER_URL });
const client = new ApolloClient({
  link,
  cache: new InMemoryCache()
});

module.exports = client