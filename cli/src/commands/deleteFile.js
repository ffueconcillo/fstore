
const {Command, flags} = require('@oclif/command');
const gql = require('graphql-tag');
const client = require('../apollo');

class DeleteFileCommand extends Command {
  async run() {
    const { flags } = this.parse(DeleteFileCommand);
    const filename = flags.file;

    if (!process.env.FSTORE_SERVER_URL) {
      this.error('Environment variable FSTORE_SERVER_URL is not set.');
    }

    this.log(`Server URL: ${process.env.FSTORE_SERVER_URL}\n`);
    
    client.mutate({
      mutation: gql`
        mutation{
          deleteFile(filename: "${filename}")
        }
      `,
    })
    .then(data => {
      if (data.data.deleteFile) {
        this.log(`${filename} successfully deleted.`)
      }
    })
    .catch(error => this.log(error.graphQLErrors[0].message));
  }
}

DeleteFileCommand.description = `Deletes a file in FileBucket
...
Deletes a single file in FileBucket
`

DeleteFileCommand.flags = {
  file: flags.string({char: 'f', description: 'Name of file to delete'}),
}

module.exports = DeleteFileCommand
