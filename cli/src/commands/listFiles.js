
const {Command, flags} = require('@oclif/command');
const gql = require('graphql-tag');
const client = require('../apollo');
class ListFilesCommand extends Command {
  async run() {

    if (!process.env.FSTORE_SERVER_URL) {
      this.error('Environment variable FSTORE_SERVER_URL is not set.');
    }

    this.log(`Server URL: ${process.env.FSTORE_SERVER_URL}\n`);
    
    client.query({
      query: gql`
        query {
          files
        }
      `,
    })
    .then(data => {
      this.log('============\nFiles on FileBucket::\n============');
      data.data.files.forEach((file) => this.log(`* ${file}`))
      this.log('\n');
    })
    .catch(error => this.log(error));
  }
}

ListFilesCommand.description = `Lists all files in FileBucket
...
Lists all files in FileBucket
`

module.exports = ListFilesCommand
