
const {Command, flags} = require('@oclif/command');
const { existsSync } = require('fs');

class UploadFileCommand extends Command {
  async run() {
    const {flags} = this.parse(UploadFileCommand);
    const filePath = flags.file;
    const filename = filePath.replace(/^.*[\\\/]/, '');

    if (!process.env.FSTORE_SERVER_URL) {
      this.error('Environment variable FSTORE_SERVER_URL is not set.');
    }

    if (!existsSync(filePath)) {
      this.error(`Cannot find file ${filePath}.`);
    }

    this.log(`Server URL: ${process.env.FSTORE_SERVER_URL} \n Uploading...`);
    
    const exec = require('child_process').exec;
    const command = `curl -X POST ${process.env.FSTORE_SERVER_URL}` +
      ` -F 'operations={"operationName":"UploadFile","variables":{"file":null},"query":"mutation UploadFile($file: Upload!) {\\n  uploadFile(file: $file)\\n}\\n"}'` +
      ` -F 'map={"1":["variables.file"]}'` +
      ` -F 1=@${filePath}`;

    exec(command, (error, stdout, stderr) => {
      if (error) {
        this.error(`Error countered. ${filename} was not uploaded.\n`)
      } 

      if (JSON.parse(stdout).data.uploadFile) {
        this.log(`${filename}  successfully uploaded.\n`)
      }
      
    });
  }
}

UploadFileCommand.description = `Uploads a file to FileBucket
...
Uploads a single file to FileBucket
`

UploadFileCommand.flags = {
  file: flags.string({char: 'f', description: 'Path of file to upload'}),
}

module.exports = UploadFileCommand





