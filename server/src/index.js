const { ApolloServer } = require("apollo-server-express");
const express = require("express");
const resolvers = require("./resolvers");
const typeDefs = require("./typeDefs");

const server = new ApolloServer({ typeDefs, resolvers });
const app = express();
server.applyMiddleware({ app });


const FSTORE_SERVER_PORT = process.env.PORT || 4000;
const FSTORE_SERVER_URL = process.env.FSTORE_SERVER_URL || `http://localhost:${FSTORE_SERVER_PORT}/`;

app.listen(FSTORE_SERVER_PORT, () => {
  console.log(`🚀  Server ready at ${FSTORE_SERVER_URL}`);
});