const path = require("path");
const { createWriteStream } = require("fs");
const { Storage } = require("@google-cloud/storage");


const gcpStorage = new Storage({
  keyFilename: path.join(__dirname, "../fstore-d6b6f00c08dd.json"),
  projectId: "fstore-265509"
});

const fileBucket = gcpStorage.bucket('fstore-files');

const resolvers = {
  Query: { 
    files: async() => {
      const [ files ] = await fileBucket.getFiles();
      return files.map(file => file.metadata.name)
    }
  },
  Mutation: {
    uploadFile: async (_, { file }) => {
      const { createReadStream, filename } = await file;

      await new Promise(res =>
        createReadStream()
          .pipe(
            fileBucket.file(filename).createWriteStream({
              resumable: false,
              gzip: true
            })
          )
          .on("finish", res)
      );

      return true;
    },

    deleteFile: async (_, { filename }) => 
      await fileBucket.file(filename).delete()
        .then(() => {
          console.log(filename + " successfully deleted");
          return true;
        })

  }
};

module.exports = resolvers;