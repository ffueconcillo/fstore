import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { CssBaseline, Typography } from '@material-ui/core';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { AppConfig } from "./AppConfig"
import { Files } from "./components/Files";
import { Trash } from "./components/Files/trash";
import { FsAppBar } from "./components/FsAppBar"
import { FsDrawer } from "./components/FsDrawer"
import { Login } from "./components/Login"

const drawerWidth = 240;
const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  contentHeader: {
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));

export default function PersistentDrawerLeft() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(window.innerWidth >= 768);
  
  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <Router>
      <div className={classes.root}>
        <CssBaseline />
        <FsAppBar open={open} handleDrawerOpen={handleDrawerOpen}></FsAppBar>
        <FsDrawer open={open} handleDrawerClose={handleDrawerClose}></FsDrawer>
        <main
          className={clsx(classes.content, {
            [classes.contentShift]: open,
          })}
        >
          <div className={classes.contentHeader} />
          <Typography paragraph>
            {AppConfig.APP_DESCRIPTION}
            <br/>
            <hr/>
          </Typography>

          <Route exact path="/" component={Login}/>
          <Route path="/files" component={Files}/>
          <Route path="/trash" component={Trash}/>
          
        </main>

      </div>
    </Router>
  );
}