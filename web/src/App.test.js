import React from 'react';
import { render } from '@testing-library/react';
import App from './App2';

test('Page Renders', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/FileBucket/i);
  expect(linkElement).toBeInTheDocument();
});
