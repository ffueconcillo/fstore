import ApolloClient from "apollo-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { createUploadLink } from "apollo-upload-client";

const FSTORE_SERVER_URL = "https://server-dot-fstore-web.appspot.com/graphql";

const link = createUploadLink({ uri: FSTORE_SERVER_URL });

export const client = new ApolloClient({
  link,
  cache: new InMemoryCache()
});
