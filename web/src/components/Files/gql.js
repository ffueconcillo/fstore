import gql from "graphql-tag";

export const filesQuery = gql`
  query {
    files
  }
`;

export const uploadFileMutation = gql`
  mutation UploadFile($file: Upload!) {
    uploadFile(file: $file)
  }
`;

export const deleteFileMutation = gql`
  mutation($filename: String!){
    deleteFile(filename: $filename)
  }
`;