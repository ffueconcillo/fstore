import React from 'react';
import { useQuery } from "@apollo/react-hooks";
import { makeStyles } from '@material-ui/core/styles';
import { GridList, GridListTile, GridListTileBar, IconButton } from '@material-ui/core';
import { GetApp } from '@material-ui/icons';
import { Upload } from "./upload";
import { filesQuery } from "./gql";

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: '400',
    height: '450',
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
}));

export const Files = () => {
  const classes = useStyles();
  const { data, loading } = useQuery(filesQuery);

  if (loading) {
    return <div>
      <h3>Loading...</h3>
    </div>;
  }

  const downloadFile = (file) => {
    document.location.href=`https://storage.cloud.google.com/fstore-files/${file}`;
  };

  const getImage = (filename) => {
    const IMAGES = [ 'jpeg', 'jpg', 'png', 'gif', 'bmp'];
    const fileParts = filename.split('.');
    const extension = fileParts[fileParts.length - 1];
    
    if (!IMAGES.includes(extension)) {
      return './document.png';
    } else {
      return `https://storage.cloud.google.com/fstore-files/${filename}`
    }
  };

  return (
    <div className={classes.root}>
      <GridList 
        cellHeight={180} 
        className={classes.gridList} 
        cols={ window.innerWidth >= 480 ? 6 : 2 }
        
      >
        {data.files.map(tile => (
          <GridListTile key={tile}>
            <img src={getImage(tile)} alt={tile} />X
            <GridListTileBar
              title={tile}
              actionIcon={
                <IconButton aria-label={`Delete ${tile}`} className={classes.icon}>
                  <GetApp onClick={() => downloadFile(tile) }/>
                </IconButton>
              }
            />
          </GridListTile>
        ))}
        <Upload />
      </GridList>
    </div>
  );
}