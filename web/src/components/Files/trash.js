import React from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { makeStyles } from '@material-ui/core/styles';
import { 
  IconButton,
  GridList, 
  GridListTile, 
  GridListTileBar,
} from '@material-ui/core';
import { Delete } from '@material-ui/icons';
import { filesQuery, deleteFileMutation } from "./gql";

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: '400',
    height: '450',
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)',
  },
}));

export const Trash = () => {
  const classes = useStyles();
  const { data, loading } = useQuery(filesQuery);

  const [deleteFile] = useMutation(deleteFileMutation, {
    refetchQueries: [{ query: filesQuery }]
  });

  if (loading) {
    return <div>loading...</div>;
  }
  
  const getImage = (filename) => {
    const IMAGES = [ 'jpeg', 'jpg', 'png', 'gif', 'bmp'];
    const fileParts = filename.split('.');
    const extension = fileParts[fileParts.length - 1];
    
    if (!IMAGES.includes(extension)) {
      return './document.png';
    } else {
      return `https://storage.cloud.google.com/fstore-files/${filename}`
    }
  };

  return (
    <div className={classes.root}>
      <GridList 
        cellHeight={180} 
        className={classes.gridList} 
        cols={ window.innerWidth >= 480 ? 6 : 2 }
        spacing={10}
      >
        {data.files.map(tile => (
          <GridListTile key={tile}>
            <img src={getImage(tile)} alt={tile} />X
            <GridListTileBar
              title={tile}
              actionIcon={
                <IconButton aria-label={`Delete ${tile}`} className={classes.icon}>
                  <Delete onClick={() => deleteFile({ variables: { filename: tile }})}/>
                </IconButton>
              }
            />
          </GridListTile>
        ))}
      </GridList>
    </div>
  );
}