import React, { useCallback } from "react";
import { useDropzone } from "react-dropzone";
import { useMutation } from "@apollo/react-hooks";
import { filesQuery, uploadFileMutation } from "./gql";

import { GridListTile, IconButton } from '@material-ui/core';
import { Publish } from '@material-ui/icons';

export const Upload = () => {
  const [uploadFile] = useMutation(uploadFileMutation, {
    refetchQueries: [{ query: filesQuery }]
  });
  const onDrop = useCallback(
    ([file]) => {
      uploadFile({ variables: { file } });
    },
    [uploadFile]
  );
  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  return (
    <div {...getRootProps()}>
      <GridListTile key="upload-file">
        <IconButton style={{ "text-align": "center", margin: "30px" }}>
          <Publish style={{ fontSize: 60 }}/>
          <input {...getInputProps()} />
        </IconButton>
      </GridListTile>
    </div>
  );
};
