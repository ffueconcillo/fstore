import React from 'react';
import clsx from 'clsx';
import {
  AppBar,
  Badge,
  IconButton, 
  InputBase,
  Toolbar, 
  Typography 
} from '@material-ui/core';
import { Menu, Search, Notifications, AccountCircle } from '@material-ui/icons';
import { AppConfig } from "../../AppConfig"
import { useStyles } from "./styles";

export const FsAppBar = (props) => {
  const classes = useStyles();
  
  return (
    <AppBar
        position="fixed" 
        className={clsx(classes.appBar, {
          [classes.appBarShift]: props.open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={props.handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, props.open && classes.hide)}
          >
            <Menu />
          </IconButton>
          <Typography variant="h6" noWrap>
            {AppConfig.APP_TITLE}
          </Typography>
          <div className={classes.grow} />

          <div className={classes.search}>
            <div className={classes.searchIcon}>
              <Search />
            </div>
            <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
            />
          </div>
          
          <div className={classes.sectionDesktop}>
            
            <IconButton aria-label="show 17 new notifications" color="inherit">
              <Badge color="secondary">
                <Notifications />
              </Badge>
            </IconButton>
            <IconButton
              edge="end"
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
  )
}