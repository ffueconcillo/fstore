import React from 'react';
import { Link } from 'react-router-dom';

import { makeStyles, useTheme } from '@material-ui/core/styles';
import { 
  Divider,
  Drawer, 
  IconButton, 
  List,
  ListItem,
  ListItemIcon, 
  ListItemText, 
} from '@material-ui/core';

import { ChevronLeft, ChevronRight, Description, Delete } from '@material-ui/icons';

const drawerWidth = 240;
const useStyles = makeStyles(theme => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: 'flex-end',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: -drawerWidth,
  },
  contentShift: {
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  },
}));


export const FsDrawer = (props) => {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <Drawer
        className={classes.drawer}
        variant="persistent"
        anchor="left"
        open={props.open}
        classes={{
          paper: classes.drawerPaper,
        }}
      > 
        <div className={classes.drawerHeader}>
          <IconButton onClick={props.handleDrawerClose}>
            {theme.direction === 'ltr' ? <ChevronLeft /> : <ChevronRight />}
          </IconButton>
        </div>
        <Divider />
        <List>
          <Link button to="/files" style={{ textDecoration: 'none', color: 'gray' }}>
            <ListItem button key="files">
              <ListItemIcon><Description fontSize="medium"/></ListItemIcon>
              <ListItemText primary="Files" />
            </ListItem>
          </Link>
        </List>
        <Divider />
        <List>
          <Link button to="/trash" style={{ textDecoration: 'none', color: 'gray' }}>
            <ListItem button key="trash">
              <ListItemIcon><Delete fontSize="medium"/></ListItemIcon>
              <ListItemText primary="Trash" />
            </ListItem>
            </Link>
        </List>
      </Drawer>
  )
}