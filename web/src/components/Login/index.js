import React from 'react';
import { GoogleLogin } from 'react-google-login';
import { AppConfig } from "../../AppConfig"
import { useHistory } from "react-router-dom";

export const Login = () => {
  const history = useHistory();
  const responseGoogle = (response) => {
    console.log(response);
    if (response.googleId) {
      history.push("/files");
    }
  };

  return (
    <div>
      <h2>Login</h2>
      <GoogleLogin
        clientId={AppConfig.GOOGLE_API_CLIENT_ID}
        buttonText="Login with Google"
        onSuccess={responseGoogle}
        onFailure={responseGoogle}
        cookiePolicy={'single_host_origin'}
      />
    </div>
  );
}